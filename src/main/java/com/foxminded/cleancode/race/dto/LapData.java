package com.foxminded.cleancode.race.dto;

import java.time.LocalDateTime;

public class LapData {
	private String key;
	private LocalDateTime time;

	public LapData(String key, LocalDateTime time) {
		this.key = key;
		this.time = time;
	}

	public String getKey() {
		return key;
	}

	public LocalDateTime getTime() {
		return time;
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + key.hashCode();
		result = 31 * result + time.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || this.getClass() != obj.getClass())
			return false;
		if (this == obj)
			return true;
		LapData lapdata = (LapData) obj;
		return this.key.equals(lapdata.getKey()) && this.time.equals(lapdata.getTime());
	}
}