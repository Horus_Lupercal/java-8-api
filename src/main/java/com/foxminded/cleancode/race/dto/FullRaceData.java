package com.foxminded.cleancode.race.dto;

import java.time.Duration;

public class FullRaceData {
	private String racerName;
	private String team;
	private Duration lapTime;

	public FullRaceData(String name, String team, Duration lapTime) {
		this.racerName = name;
		this.team = team;
		this.lapTime = lapTime;
	}

	public String getRacerName() {
		return racerName;
	}

	public String getTeam() {
		return team;
	}

	public Duration getLapTime() {
		return lapTime;
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + racerName.hashCode();
		result = 31 * result + team.hashCode();
		result = 31 * result + lapTime.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || this.getClass() != obj.getClass())
			return false;
		if (this == obj)
			return true;
		FullRaceData fullRace = (FullRaceData) obj;
		return this.racerName.equals(fullRace.getRacerName()) && this.team.equals(fullRace.getTeam())
				&& this.lapTime.equals(fullRace.getLapTime());
	}
}
