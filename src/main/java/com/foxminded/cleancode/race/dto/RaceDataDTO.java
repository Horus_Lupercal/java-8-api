package com.foxminded.cleancode.race.dto;

import java.util.ArrayList;
import java.util.List;

public class RaceDataDTO {
	
	private List<LapData> startLapsData = new ArrayList<>();
	private List<LapData> finishLapsData = new ArrayList<>();
	private List<RacerData> racersData = new ArrayList<>();
	
	public List<LapData> getStartLapsData() {
		return startLapsData;
	}
	
	public void setStartLapsData(List<LapData> startLapsData) {
		this.startLapsData = startLapsData;
	}
	
	public List<LapData> getFinishLapsData() {
		return finishLapsData;
	}
	
	public void setFinishLapsData(List<LapData> finishLapsData) {
		this.finishLapsData = finishLapsData;
	}
	
	public List<RacerData> getRacersData() {
		return racersData;
	}
	
	public void setRacersData(List<RacerData> racersData) {
		this.racersData = racersData;
	}
}
