package com.foxminded.cleancode.race.dto;

public class RacerData {
	private String key;
    private String name;
    private String team;

    public RacerData(String key, String name, String team) {
        this.key = key;
        this.name = name;
        this.team = team;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public String getTeam() {
        return team;
    }
    
    @Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + key.hashCode();
		result = 31 * result + name.hashCode();
		result = 31 * result + team.hashCode();
		return result;
	}
    
    @Override
    public boolean equals(Object obj) {
    	if (obj == null || this.getClass() != obj.getClass()) return false;
		if (this == obj) return true;
		RacerData race = (RacerData) obj;
		return this.key.equals(race.getKey()) && 
				this.name.equals(race.getName()) &&
				this.team.equals(race.getTeam());
    }
}
