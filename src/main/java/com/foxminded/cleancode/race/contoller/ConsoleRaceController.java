package com.foxminded.cleancode.race.contoller;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.foxminded.cleancode.race.dto.FullRaceData;
import com.foxminded.cleancode.race.service.RaceService;
import com.foxminded.cleancode.race.view.ConsoleRaceView;

public class ConsoleRaceController {
	private final AtomicInteger counter = new AtomicInteger(1);
	private RaceService raceService;
	private ConsoleRaceView raceview = new ConsoleRaceView();
	

	public ConsoleRaceController(RaceService raceService) {
		this.raceService = raceService;
		
	}

	public void findWinner() {
		String toConsole = collectedString(raceService.findWinner());
		raceview.printResult(toConsole);
	}
	
	private String collectedString(List<FullRaceData> collectedData) {
		return collectedData.stream().map(fullRaceData -> {
			String formatRow = String.format("%s | %s | %s", 
					formatDataGivenMaxDataLength(counter.getAndIncrement() + ".".concat(fullRaceData.getRacerName())),
					formatDataGivenMaxDataLength(fullRaceData.getTeam()),
					formatDuration(fullRaceData.getLapTime()));

			formatRow = addRowSeparator(formatRow, counter);

			return formatRow;
		}).collect(Collectors.joining("\n"));
	}
	
	private String addRowSeparator(String formatRow, AtomicInteger counter) {

	    if(counter.get() == 16) {
	        formatRow = formatRow.concat("\n").concat(repeatDash(formatRow.length()));
	    }

	    return formatRow;
	}
	
	private String repeatDash(int count) {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < count; i++) {
			result.append("-");
		}
		return result.toString();
	}
	
	private String formatDataGivenMaxDataLength(String data) {
	    return String.format("%-30s", data);
	}
	
	private String formatDuration(Duration duration) {
	    long minutes = duration.toMinutes();
	    long seconds = duration.getSeconds() - (60 * minutes);
	    long millis = duration.toMillis() - (1000 * duration.getSeconds());
	    return String.format("%d:%02d:%03d", minutes, seconds, millis);
	}
}
