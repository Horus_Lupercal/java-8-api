package com.foxminded.cleancode.race.contoller;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.foxminded.cleancode.race.dto.FullRaceData;
import com.foxminded.cleancode.race.service.RaceService;
import com.foxminded.cleancode.race.view.HtmlRaceView;

public class HtmlRaceController {
	private final AtomicInteger counter = new AtomicInteger(1);
	private RaceService raceService;
	private HtmlRaceView raceview = new HtmlRaceView();
	
	public HtmlRaceController(RaceService raceService) {
		this.raceService = raceService;
	}
	
	public void findWinner() {
		printResult(raceService.findWinner());
	}
	
	private void printResult(List<FullRaceData> collectedData) {
		List<String[]> result = collectedData.stream().map(this::collectedArray)
				.collect(Collectors.toList());
		raceview.printResult(result);
	}
	
	private String[] collectedArray(FullRaceData racer) {
		String[] result = new String[4];
		result[0] = "" + counter.getAndIncrement();
		result[1] = racer.getRacerName();
		result[2] = racer.getTeam();
		result[3] = formatDuration(racer.getLapTime());
		return result;
	}
	
	private String formatDuration(Duration duration) {
	    long minutes = duration.toMinutes();
	    long seconds = duration.getSeconds() - (60 * minutes);
	    long millis = duration.toMillis() - (1000 * duration.getSeconds());
	    return String.format("%d:%02d:%03d", minutes, seconds, millis);
	}
}
