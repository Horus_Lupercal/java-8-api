package com.foxminded.cleancode.race;

import java.io.IOException;

import com.foxminded.cleancode.race.contoller.ConsoleRaceController;
import com.foxminded.cleancode.race.contoller.HtmlRaceController;
import com.foxminded.cleancode.race.service.RaceDataService;
import com.foxminded.cleancode.race.service.RaceService;


public class RaceApplication {
	
	public static void main(String[] args) throws IOException {
		RaceDataService racedataService = new RaceDataService();
		RaceService service = new RaceService(racedataService.readDataResources());
		
		ConsoleRaceController console = new ConsoleRaceController(service);
		console.findWinner();
		
		HtmlRaceController html = new HtmlRaceController(service);
		html.findWinner();
	}
}


