package com.foxminded.cleancode.race.service;

import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.foxminded.cleancode.race.dto.LapData;
import com.foxminded.cleancode.race.dto.RaceDataDTO;
import com.foxminded.cleancode.race.dto.RacerData;

public class RaceDataService {

	public RaceDataDTO readDataResources() {
		RaceDataDTO raceDto = new RaceDataDTO();
		String[] dataFilses = new String[] { "start.log", "end.log", "abbreviations.txt" };
		for (String file : dataFilses) {
			Path path = null;
			try {
				path = Paths.get(getClass().getClassLoader().getResource("application.properties\\" + file).toURI());
				fillDto(raceDto, path);
	
			} catch (URISyntaxException e1) {
				e1.printStackTrace();
			}
		}
		return raceDto;
	}

	private void fillDto(RaceDataDTO dto, Path path) {
		try (Stream<String> streamFromFile = Files.lines(path)) {
			List<String> list = streamFromFile.collect(Collectors.toList());

			switch (path.getFileName().toString()) {
				case "end.log":
					dto.setFinishLapsData(toLapData(list));
					break;
				case "start.log":
					dto.setStartLapsData(toLapData(list));
					break;
				case "abbreviations.txt":
					dto.setRacersData(toRacerData(list));
					break;					 
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private List<LapData> toLapData(List<String> list) {
		List<LapData> ladData = list.stream().map(line -> {
			
			String key = line.substring(0, 3);
			String deleteLitters = line.replaceAll("[A-Za-z]*", "");
			String time = deleteLitters.replace("_", "T");

			return new LapData(key, LocalDateTime.parse(time));
			}
		).collect(Collectors.toList());
		return ladData;
	}
	
	private List<RacerData> toRacerData(List<String> list) {
		List<RacerData> racersData = list.stream().map(line -> {
			
			String[] lines = line.split("_");
			
			return new RacerData(lines[0], lines[1], lines[2]);
			}
		).collect(Collectors.toList());
		return racersData;
	}
}
