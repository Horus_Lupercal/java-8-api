package com.foxminded.cleancode.race.service;

import com.foxminded.cleancode.race.dto.FullRaceData;
import com.foxminded.cleancode.race.dto.LapData;
import com.foxminded.cleancode.race.dto.RaceDataDTO;
import com.foxminded.cleancode.race.dto.RacerData;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class RaceService {
	
	private RaceDataDTO raceDataDto; 
	
	public RaceService(RaceDataDTO raceDataDto) {
		this.raceDataDto = raceDataDto;
	}
	
	public List<FullRaceData> findWinner() {
		return raceDataDto.getRacersData()
	            .stream()
	            .map(this::collectFullRaceData)
	            .sorted(Comparator.comparing(FullRaceData::getLapTime))
	            .collect(Collectors.toList());
	}
	
	private FullRaceData collectFullRaceData(RacerData mainData) {
	    LapData start = findLapDataByKey(raceDataDto.getStartLapsData(), mainData.getKey());
	    LapData finish = findLapDataByKey(raceDataDto.getFinishLapsData(), mainData.getKey());

	    return new FullRaceData(mainData.getName(), mainData.getTeam(), Duration.between(start.getTime(), finish.getTime()));
	}

	private LapData findLapDataByKey(List<LapData> startLapsData, String key) {
	    return startLapsData.stream().filter(lapData -> lapData.getKey().equals(key)).findAny().orElse(new LapData("null", LocalDateTime.now()));
	}
}
