package com.foxminded.cleancode.race.view;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class HtmlRaceView {

	public void printResult(List<String[]> result) {
		String resultPath = "D:\\foxminded\\repository\\java-8-api\\src\\main\\resources\\application.properties\\result";
		File file = new File(resultPath);
		file.mkdir();
		String filePath = resultPath + "\\result.html";
		try (PrintWriter writer = new PrintWriter(filePath, "UTF-8")) {
			StringBuilder html = new StringBuilder(
					"<link rel=\"stylesheet\" href=\"D:\\foxminded\\repository\\java-8-api\\src\\main\\resources\\application.properties\\css\\result - styles.css\">");
			html.append("<table>");
			html.append("<tr>" + "<th>Place</th>" + "<th>Name</th>" + "<th>Car</th>" + "<th>Time</th>" + "</tr>");
			for (String[] racer : result) {
				if (Integer.parseInt(racer[0]) <= 15) {
					html.append("<tr class='win'>");
				} else {
					html.append("<tr class='lose'>");
				}
				html.append(String.format("<th>%s</th>" + "<td>%s</td>" + "<td>%s</td>" + "<td>%s</td>" + "</tr>",
						racer[0], racer[1], racer[2], racer[3]));
			}
			html.append("</table>");
			writer.print(html);
			Desktop.getDesktop().open(new File(filePath));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
