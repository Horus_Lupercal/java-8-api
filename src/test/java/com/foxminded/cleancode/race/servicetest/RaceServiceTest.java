package com.foxminded.cleancode.race.servicetest;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import com.foxminded.cleancode.race.dto.FullRaceData;
import com.foxminded.cleancode.race.dto.LapData;
import com.foxminded.cleancode.race.dto.RaceDataDTO;
import com.foxminded.cleancode.race.dto.RacerData;
import com.foxminded.cleancode.race.service.RaceService;

public class RaceServiceTest {
	private RaceService race;
	
	@Test
	public void testfindWinnerCorrectValueExceptedisOk() throws IOException {
		RaceDataDTO correctdto = getRaceDatadto();
		race = new RaceService(correctdto);
		List<FullRaceData> currectList = race.findWinner();
		List<FullRaceData> exceptedList = getRace();
		Assert.assertEquals(currectList.get(0), exceptedList.get(0));
	}
	
	private RaceDataDTO getRaceDatadto() {
		RaceDataDTO dto = new RaceDataDTO();
		dto.getStartLapsData().add(new LapData("SVF", LocalDateTime.parse("2018-05-24T12:02:58.917")));
		dto.getFinishLapsData().add(new LapData("SVF", LocalDateTime.parse("2018-05-24T12:04:03.332")));
		dto.getRacersData().add(new RacerData("SVF", "Sebastian Vettel", "FERRARI"));
		return dto;
	}
	
	private List<FullRaceData> getRace() {
		List<FullRaceData> race = new ArrayList<>();
		race.add(new FullRaceData("Sebastian Vettel", "FERRARI", Duration.parse("PT1M4.415S")));
		return race;
	}
}
