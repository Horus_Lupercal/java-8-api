package com.foxminded.cleancode.race.servicetest;

import java.time.LocalDateTime;
import java.util.Arrays;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import com.foxminded.cleancode.race.dto.LapData;
import com.foxminded.cleancode.race.dto.RaceDataDTO;
import com.foxminded.cleancode.race.dto.RacerData;
import com.foxminded.cleancode.race.service.RaceDataService;

public class RaceDataServiceTest {

	private RaceDataService racedata = new RaceDataService();

	@Test
	public void testReadDataCorrectValueExeptedIsOk() {

		RaceDataDTO correctDto = racedata.readDataResources();

		RaceDataDTO exptedDto = getdto();
		
		Assertions.assertThat(correctDto).isEqualToComparingFieldByField(exptedDto);
	}

	private RaceDataDTO getdto() {
		RaceDataDTO dto = new RaceDataDTO();
		dto.getStartLapsData()
				.addAll(Arrays.asList(
						new LapData("SVF", LocalDateTime.parse("2018-05-24T12:02:58.917")),
						new LapData("NHR", LocalDateTime.parse("2018-05-24T12:02:49.914")),
						new LapData("FAM", LocalDateTime.parse("2018-05-24T12:13:04.512")),
						new LapData("KRF", LocalDateTime.parse("2018-05-24T12:03:01.250")),
						new LapData("SVM", LocalDateTime.parse("2018-05-24T12:18:37.735")),
						new LapData("MES", LocalDateTime.parse("2018-05-24T12:04:45.513")),
						new LapData("LSW", LocalDateTime.parse("2018-05-24T12:06:13.511")),
						new LapData("BHS", LocalDateTime.parse("2018-05-24T12:14:51.985")),
						new LapData("EOF", LocalDateTime.parse("2018-05-24T12:17:58.810")),
						new LapData("RGH", LocalDateTime.parse("2018-05-24T12:05:14.511")),
						new LapData("SSW", LocalDateTime.parse("2018-05-24T12:16:11.648")),
						new LapData("KMH", LocalDateTime.parse("2018-05-24T12:02:51.003")),
						new LapData("PGS", LocalDateTime.parse("2018-05-24T12:07:23.645")),
						new LapData("CSR", LocalDateTime.parse("2018-05-24T12:03:15.145")),
						new LapData("SPF", LocalDateTime.parse("2018-05-24T12:12:01.035")),
						new LapData("DRR", LocalDateTime.parse("2018-05-24T12:14:12.054")),
						new LapData("LHM", LocalDateTime.parse("2018-05-24T12:18:20.125")),
						new LapData("CLS", LocalDateTime.parse("2018-05-24T12:09:41.921")),
						new LapData("VBM", LocalDateTime.parse("2018-05-24T12:00:00.000"))));

		dto.getFinishLapsData()
				.addAll(Arrays.asList(
						new LapData("MES", LocalDateTime.parse("2018-05-24T12:05:58.778")),
						new LapData("RGH", LocalDateTime.parse("2018-05-24T12:06:27.441")),
						new LapData("SPF", LocalDateTime.parse("2018-05-24T12:13:13.883")),
						new LapData("LSW", LocalDateTime.parse("2018-05-24T12:07:26.834")),
						new LapData("DRR", LocalDateTime.parse("2018-05-24T12:15:24.067")),
						new LapData("NHR", LocalDateTime.parse("2018-05-24T12:04:02.979")),
						new LapData("CSR", LocalDateTime.parse("2018-05-24T12:04:28.095")),
						new LapData("KMH", LocalDateTime.parse("2018-05-24T12:04:04.396")),
						new LapData("BHS", LocalDateTime.parse("2018-05-24T12:16:05.164")),
						new LapData("SVM", LocalDateTime.parse("2018-05-24T12:19:50.198")),
						new LapData("KRF", LocalDateTime.parse("2018-05-24T12:04:13.889")),
						new LapData("VBM", LocalDateTime.parse("2018-05-24T12:01:12.434")),
						new LapData("SVF", LocalDateTime.parse("2018-05-24T12:04:03.332")),
						new LapData("EOF", LocalDateTime.parse("2018-05-24T12:19:11.838")),
						new LapData("PGS", LocalDateTime.parse("2018-05-24T12:08:36.586")),
						new LapData("SSW", LocalDateTime.parse("2018-05-24T12:17:24.354")),
						new LapData("FAM", LocalDateTime.parse("2018-05-24T12:14:17.169")),
						new LapData("CLS", LocalDateTime.parse("2018-05-24T12:10:54.750")),
						new LapData("LHM", LocalDateTime.parse("2018-05-24T12:19:32.585"))));

		dto.getRacersData()
				.addAll(Arrays.asList(
						new RacerData("DRR", "Daniel Ricciardo", "RED BULL RACING TAG HEUER"), 
						new RacerData("SVF", "Sebastian Vettel", "FERRARI"),
						new RacerData("LHM", "Lewis Hamilton", "MERCEDES"), 
						new RacerData("KRF", "Kimi Raikkonen", "FERRARI"), 
						new RacerData("VBM", "Valtteri Bottas", "MERCEDES"),
						new RacerData("EOF", "Esteban Ocon", "FORCE INDIA MERCEDES"), 
						new RacerData("FAM", "Fernando Alonso", "MCLAREN RENAULT"),
						new RacerData("CSR", "Carlos Sainz", "RENAULT"), 
						new RacerData("SPF", "Sergio Perez", "FORCE INDIA MERCEDES"),
						new RacerData("PGS", "Pierre Gasly", "SCUDERIA TORO ROSSO HONDA"), 
						new RacerData("NHR", "Nico Hulkenberg", "RENAULT"),
						new RacerData("SVM", "Stoffel Vandoorne", "MCLAREN RENAULT"), 
						new RacerData("SSW", "Sergey Sirotkin", "WILLIAMS MERCEDES"),
						new RacerData("CLS", "Charles Leclerc", "SAUBER FERRARI"), 
						new RacerData("RGH", "Romain Grosjean", "HAAS FERRARI"),
						new RacerData("BHS", "Brendon Hartley", "SCUDERIA TORO ROSSO HONDA"), 
						new RacerData("MES", "Marcus Ericsson", "SAUBER FERRARI"),
						new RacerData("LSW", "Lance Stroll", "WILLIAMS MERCEDES"), 
						new RacerData("KMH", "Kevin Magnussen", "HAAS FERRARI")));
		return dto;
	}
}
