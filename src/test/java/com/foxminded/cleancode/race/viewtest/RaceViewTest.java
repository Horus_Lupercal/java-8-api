package com.foxminded.cleancode.race.viewtest;

import java.io.File;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.foxminded.cleancode.race.view.HtmlRaceView;

public class RaceViewTest {
	
	private HtmlRaceView view = new HtmlRaceView();
	
	@Test
	public void TestResultToHtmlCorrectDataExceptedFileExist() {
		List<String[]> result = getMap();
		view.printResult(result);
		File file = new File("D:\\foxminded\\repository\\java-8-api\\src\\main\\resources\\application.properties\\result\\result.html");
		Assertions.assertTrue(file.exists());
		file.delete();
	}
	
	private List<String[]> getMap() {
		List<String[]> result = new ArrayList<>();
		String[] racer = new String[4];
		racer[0] = "1";
		racer[1] = "Sebastian Vettel";
		racer[2] = "FERRARI";
		racer[3] = "1:04:415";
		result.add(racer);
		return result;
	}
}
